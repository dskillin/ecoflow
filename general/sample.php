<?php

$serials = array (
        array ("system" => "Delta 2", "serial" => "R331ZAB4ZEXXXXXX"),
        array ("system" => "Delta 2", "serial" => "R331ZAB4ZEXXXXXX"),
        array ("system" => "Delta Pro", "serial" => "DCABZ8ZEXXXXXX"),
        array ("system" => "Delta Pro", "serial" => "DCABZ8ZEXXXXXX"),
        array ("system" => "River Pro", "serial" => "P2ABZ7ZDXXXXXX"),
        array ("system" => "River Mini", "serial" => "R5ABZ5ZXXXXXXX"),
        array ("system" => "River Mini", "serial" => "R5ABZ5ZXXXXXXX"),
        array ("system" => "River Mini", "serial" => "R5ABZ5ZXXXXXXX")

);

$appkey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
$secretkey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";


foreach ( $serials as $entry ) {

        $serial = $entry['serial'];
        $system = $entry['system'];

        $query = "?sn=" . $serial;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://api.ecoflow.com/iot-service/open/api/device/queryDeviceQuota" . $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        $headers = [
                'Content-Type: application/json',
                'appKey: ' . $appkey,
                'secretKey: ' . $secretkey
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = trim(curl_exec($ch));
        curl_close ($ch);

        $status = json_decode($server_output);

        $wattsIn = $status->data->wattsInSum;
        $wattsOut = $status->data->wattsOutSum;
        $soc = $status->data->soc;
        $remaining = $status->data->remainTime;

        echo '' . time() . ",$wattsIn,$wattsOut,$soc,$remaining,$system\n";
}

?>
